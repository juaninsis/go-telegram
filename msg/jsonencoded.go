package msg

type (

	//it's the msg returned to our telegram clients
	Response struct {
		ChatID int64  `json:"chat_id"`
		Text   string `json:"text"`
		Type   string `json:"type"`
		URL    string `json:"url"`
	}

	//it's the msg format comes from telegram
	Request struct {
		Message struct {
			Chat struct {
				FirstName string `json:"first_name"`
				ID        int64  `json:"id"`
				LastName  string `json:"last_name"`
				Type      string `json:"type"`
			} `json:"chat"`
			Date     int64    `json:"date"`
			Entities []Entity `json:"entities"`
			From     struct {
				FirstName    string `json:"first_name"`
				ID           int64  `json:"id"`
				IsBot        bool   `json:"is_bot"`
				LanguageCode string `json:"language_code"`
				LastName     string `json:"last_name"`
			} `json:"from"`
			MessageID int64  `json:"message_id"`
			Text      string `json:"text"`
		} `json:"message"`
		UpdateID int64 `json:"update_id"`
	}

	Entity struct {
		Length int64  `json:"length"`
		Offset int64  `json:"offset"`
		Type   string `json:"type"`
	}
)
