package msg

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
)

const (
	telegramBotURL = "https://api.telegram.org/bot"
	Text           = "TEXT"
	Image          = "IMAGE"
)

type client struct {
	token     string
	msg       *Request
	whiteList map[int64]struct{}
}

func New(token string, whiteChatIDs ...int64) *client {
	cli := &client{token: token}
	if len(whiteChatIDs) > 0 {
		cli.whiteList = make(map[int64]struct{}, 0)
	}
	for _, v := range whiteChatIDs {
		cli.whiteList[v] = struct{}{}
	}
	return cli
}
func (tc *client) Read(req *http.Request) (string, error) {
	if req != nil {
		if err := tc.Load(req); err != nil {
			return "", nil
		}
	}
	return tc.msg.Message.Text, nil
}

func (tc *client) Load(req *http.Request) error {
	request := &Request{}

	d := json.NewDecoder(req.Body)
	if err := d.Decode(&request); err != nil {
		fmt.Println(err, "json error")
		return err
	}
	tc.msg = request

	return nil
}

func (tc *client) GetFullName() string {
	return tc.msg.Message.Chat.LastName + ", " + tc.msg.Message.From.FirstName
}

func (tc *client) GetChatID() int64 {
	return tc.msg.Message.Chat.ID
}
func (tc *client) Write(chatID int64, txt, msgType string) error {
	if !tc.isAllowed(chatID) {
		return errors.New("method_not_allowed")
	}
	reqBody := &Response{
		ChatID: chatID,
		Text:   txt,
		Type:   getType(msgType),
	}

	reqBytes, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	res, err := http.Post(telegramBotURL+tc.token+"/sendMessage", "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("unexpected status" + res.Status)
	}

	return nil
}

func (tc *client) isAllowed(chatID int64) bool {
	if len(tc.whiteList) <= 0 {
		return true
	}
	_, ok := tc.whiteList[chatID]
	return ok
}

func getType(msgType string) string {
	switch strings.ToUpper(msgType) {
	case Image:
		return Image
	default:
		return Text
	}
}
